```bash
docker run --rm \
    -p 127.0.0.1:5672:5672 \
    -p 127.0.0.1:8080:15672 \
    --hostname rabbit \
    --name rabbit rabbitmq:3-management

docker run --rm \
    -p 127.0.0.1:6379:6379 \
    --name redis \
    redis:alpine redis-server

docker run --rm \
    -p 127.0.0.1:5555:5555 \
    --name flower \
    --link redis:redishost \
    osminogin/flower --broker redis://redishost:6379/0
```


```bash
pip install --user pipenv
pipenv install
pipenv shell
```

```bash
celery -l info -A dot_tp worker -B
```